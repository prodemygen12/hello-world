package com.prodemy.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Hello
 */
@WebServlet("/Hello")
public class Hello extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = res.getWriter();
		
		out.print("<html><body>");
		int angka1 = 1;
		int angka2 = 1;
		int fibonacci = 0;
		for(int i=0; i<5; i++) {
			fibonacci = angka1+angka2;
			out.print(angka1 + "<br>");
			angka1= angka2;
			angka2 = fibonacci;
		}
		out.print("<h3>Bilangan Fibonacci</h3>");
		out.print("</body></html>");
	}

}
